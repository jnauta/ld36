var myTexts = {
	score : 0,
	sculpture0: 'Leonardo has so much sculpting to do! To speed things up, he has invented a Helisculptor. <br><br>Try to match the template in the top right corner. <br><br> <span class = "boldMessage">Move: Arrow keys<br>Smash: Space<br> Skip text: Enter<br>Restart: R</span>',
	sculptureStart: 'Good luck',

	levelFinished: "Time's up!" ,
	levelPassed: 'Press <span class = "boldMessage">Enter</span> to make the next sculpture.' ,
	levelFailed: 'Hm, well... That <span class = "italic">is</span> a pretty rock, but try to aim for at least 60% accuracy. <br> <br>Press Enter to try again.' ,
	finished: 'You have completed all sculptures!<br><br><span class = "boldMessage">Total score: ',
	finished2: 'Press <span class = "boldMessage">Enter</span> to play another run.',
	helpText: 'Try to create the sculpture, before time is up. <br><br> Accurate sculpting and powerful smashing give points.<br><br> <span class = "boldMessage">Move: Arrow keys<br>Smash: Space<br> Skip text: Enter<br>Restart: R</span> ' ,
	
	
  }