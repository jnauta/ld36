var GameInstance = function(gameID, host, joiner){
  this.gameID = gameID;
  this.player1 = host;
  this.player2 = joiner;
  // initialize 2-dimensional array of the tiles
  var gameSizeTileX = 8;
  var gameSizeTileY = 8;
  this.tiles = new Array(gameSizeTileX);
  for (var i = 0 ; i < gameSizeTileX; i++ ){
    this.tiles[i] = new Array(gameSizeTileY);
  }
}