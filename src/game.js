var mutemusic = false;//false;
console.log('music muted');
var mutesound = false;
var debug = false;//true;
var playing = false;
var fontFamily1 = "Homemade Apple";//title
var fontFamily2 = "Asap"; // help text
var fontFamily3 = "Lobster Two"; // for clock
var mouseButtons = {left: 0, middle: 1, right: 2};
var sculptureIdx = 0;
var playerScore = 0;

// # Choices per layers of cake
	nrOfChoices = 5;

function saveState(state) {
	window.localStorage.setItem("gameState", JSON.stringify(state));
}
		 
function restoreState() {
	var state = window.localStorage.getItem("gameState");
	if (state) {
		return JSON.parse(state);
	} else {
		return null;
	}
}

if(restoreState()){
	
	var dummyvar = restoreState();
	if(dummyvar['highscore']){
		highscore = dummyvar['highscore'];
	}
}



Crafty.paths({
	audio: 'assets/sound/',
	images: 'assets/images/'
});

Game = {
	width: function() {
		return 800;
	},

	height: function() {
		return 600;
	},

    
    
	start: function() {
		
		
		// Start crafty and set a background color so that we can see it's working
		Crafty.init(Game.width(),Game.height(), 'cr-stage');
		Crafty.canvasLayer.init();
    //Crafty.viewport.zoom(20,0,0,100);
		ctx = Crafty.canvasLayer.context;
    Crafty.pixelart(true);
		Crafty.scene('Loading');
	},
	

};

params = {
  zLevels : {
    background: 1000,
    rock: 2000,
    rockImage: 2250,
    grass: 2500,
    player: 3000,
    rope: 4000,
    hammer: 5000,
    overlay: 6000,
    
  },

  tileSize : 60,
  edgeTiles : 2,

  levelTilesLeft : Math.ceil(Game.width() / 120),
  levelTilesRight : Math.ceil(Game.width() / 120),
  levelTilesTop : Math.ceil(Game.height() / 60),
  levelTilesBottom : 3,

  gravityForce : 0.12,
  //gravityMaxSpeed : 6,
  
  playerXAcceleration : 0.05,
  playerUpAcc: 0.2,
  playerDownAcc: 0.2,
  playerMaxLift: 1000.0,
  playerAirDragY: 0.2,
  playerAirDragX: 0.01,

  hammerSlowfactor: 0.999,
  ropeEqLength: 100,
  ropeStiffness: 0.02,

  smashSpeedThreshold: 0,

  rockScore: 5,
  airScore: 3,
  isolatedAirScore: 15,
  isolatedAirThres: 5,
  grassScore: 0,
  percentScore: 5,
  powerScore: 4,
  powerMaxV: 3,

  particleOptions : {
    maxParticles: 10,
    size: 15,
    sizeRandom: 7,
    speed: 10,
    speedRandom: 5,
    // Lifespan in frames
    lifeSpan: 49,
    lifeSpanRandom: 7,
    // Angle is calculated clockwise: 12pm is 0deg, 3pm is 90deg etc.
    angle:0,
    angleRandom: 90,
    startColour: [124, 122, 114, 1],
    startColourRandom: [15, 15, 15, 0],
    endColour: [134, 132, 124, 1],
    endColourRandom: [15, 15, 15, 0],
    // Only applies when fastMode is off, specifies how sharp the gradients are drawn
    sharpness: 50,
    sharpnessRandom: 0,
    // Random spread from origin
    spread: 10,
    // How many frames should this last
    duration: 10,
    // Will draw squares instead of circle gradients
    fastMode: true,
    gravity: { x: 0, y: 0.5 },
    // sensible values are 0-3
    jitter: 0,
    // Offset for the origin of the particles
    originOffset: {x:0, y: 0}
  },
};