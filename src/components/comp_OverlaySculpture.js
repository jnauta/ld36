Crafty.c('OverlaySculpture',{
	init: function(){
		this.requires('2D,DOM').attr({x:600, y:200,z:params.zLevels['overlay']+1});
		this.borderPx = 2; 
		this.css({'background-color': mycolors.clockBg,'border': this.borderPx + 'px solid black','border-radius': '10px' });
		this.miniTileSize = 10;
		this.h = 4*25 + 3*8;
		this.w = 4*25 + 3*8;
		this.viewportDiffX = 800 - this.borderPx*4 - this.w;
		this.viewportDiffY = 2 * this.borderPx;
		this.x = -Crafty.viewport._x + this.viewportDiffX;
		this.y = -Crafty.viewport._y + this.viewportDiffY;
		this.buttonsAlpha = 0.8;
		this.alpha = this.buttonsAlpha;
	},

	_OverlaySculpture: function(sculpture){
		for (var i = sculpture.layers.length - 1; i >= 0; i--) {
		    var layer = sculpture.layers[i];
		    if (layer.name === "rock") {
		      rockLayer = layer;
		    } 		    
		}
		
		this.miniTileSize = Math.min(40,Math.floor(Math.min(this.h,this.w)/(Math.max(sculpture.height,  sculpture.width) )));
		var startX =  this.x + this.borderPx + (this.w - sculpture.width*this.miniTileSize)/2;
		var startY = this.y + this.h + this.borderPx - sculpture.height *this.miniTileSize ;
		
		overlayTiles = new Array(sculpture.width);
		for (var col = 0; col < sculpture.width ; ++col) {
		    overlayTiles[col] = new Array();
		    for (var row = 0; row < sculpture.height ; ++row) {
		      	// create tile in sculpture
		        tileIdx = rockLayer.data[sculpture.width * (row ) + col ];
		        if (tileIdx === 1) {
		          	overlayTiles[col][row] = Crafty.e('2D,DOM').css({'background-color': mycolors.stoneExample, 'border':'1px solid '+mycolors.stoneExampleBorder})
		          		.attr({alpha: this.buttonsAlpha, h:this.miniTileSize-2,w:this.miniTileSize-2, x: startX + this.miniTileSize*col, 
		          			y: startY + row *this.miniTileSize , z: params.zLevels['overlay']+10,});
		        	this.attach(overlayTiles[col][row]); 
		        }
		      
		      // tileIdx = bgLayer.data[sculpture.width * row + col];
		      // if (tileIdx != 0) {
		      //  Crafty.e('2D, Canvas, Background')._Background(col, row).sprite((tileIdx - 1) % xTilesInSprite, Math.floor((tileIdx - 1) / xTilesInSprite));
		      // }
		    }
		}


		this.bind('EnterFrame', function() {
			// Make relative to viewport
			this.x = -Crafty.viewport._x + this.viewportDiffX;
			this.y = -Crafty.viewport._y + this.viewportDiffY;

		});	


	},

})