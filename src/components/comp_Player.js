Crafty.c('Player', {
  init: function(){
    this.addComponent('2D, Canvas, Collision, OriginCoordinates, Keyboard, KeyControls, SpriteAnimation, davinci').collision()._Moving();
    this.reel('Flying',500,0,0,4).animate('Flying',-1);
    this.origin(115, 165);
    this.collision([20,33,110,23,200,33,220,70,152,163,110,170,72,163,0,70 ]);
    this.wobbling = false;
    this.sculptureScore = 0;
    this.powerScore = 0;
    this.timeIsUp = false;
  },

  _Player: function(pos, facing) {
    this.x = pos[0];
    this.y = pos[1];
    this.z = params.zLevels['player'];
    if (facing === 'right') {
      this.unflip();
    }
    this.w = 220;
    this.h = 180;
    this.hammer = Crafty.e('Hammer')._Hammer(pos, this);
    this._KeyControls(Crafty.keys.LEFT_ARROW, Crafty.keys.RIGHT_ARROW, Crafty.keys.UP_ARROW, Crafty.keys.DOWN_ARROW, Crafty.keys.SPACE);
    this.bind('Draw', this.drawRope);
    
    // this.rope = Crafty.e('2D,Canvas,Color').attr({x:this.x+this.originX(), y:this.y+this.originY()+30, z:this.z +2, w:1000,h:1000}).color('#ff0000');
    // this.attach(this.rope);
    
    
    return this;
  },

  moveCollisionTest: function() {
    if (this.hit('Grass')) {
      this.vy = -Math.abs(this.vy);
      return true;
    }
    if (this.y < 0) {
      this.vy = Math.abs(this.vy);
      return true;
    }
    if (this.originX() < Crafty.viewport.bounds.min.x) {
      this.vx = Math.abs(this.vx);
      return true;
    }
    if (this.originX() > Crafty.viewport.bounds.max.x) {
      this.vx = -Math.abs(this.vx);
      return true;
    }
    return false;
    //console.log('checking collisions');
  },

  drawRope: function(e) {
    var ctx = e.ctx;
    e.z = 0;//params.zLevels['rope'];
    ctx.lineWidth = 3;
    ctx.strokeStyle = "black";
    ctx.beginPath();
    ctx.moveTo(this.originX(), this.originY());
    ctx.lineTo(this.hammer.originX(), this.hammer.originY());  

    ctx.stroke();
  }

});