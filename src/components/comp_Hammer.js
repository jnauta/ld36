Crafty.c('Hammer', {
  init: function(){
    this.addComponent('2D, Canvas, Moving, Collision, OriginCoordinates, hammer')._Moving();
    this.ropeStiffness = params.ropeStiffness;
    this.ropeEqLength = params.ropeEqLength;
    this.smashing = false;
    // this.origin('center');
    this.origin(15,6);
    this.collision(14,14,16,14,16,16,14,16);
  },

  _Hammer: function(pos, copter) {
    this.x = pos[0]+110;
    this.y = pos[1]+250;
    this.z = params.zLevels['hammer'];
    this.copter = copter;
    this.w = 30;
    this.h = 30;
    this.slowfactor = params.hammerSlowfactor;


    this.bind('EnterFrame', function(){
      var angle = Math.atan2(this.originY() - this.copter.originY(),this.originX() - this.copter.originX());
      this.rotation = - 90 + angle * 180/ Math.PI;
    

  
    });





    return this;
  },

  moveCollisionTest: function() {
    //console.log('checking collisions');
    if (this.smashing) {
      var rockHits = this.hit('Rock');
      if (rockHits) {
        //this.smashing = false;
        this.vx *= 0.75;
        this.vy *= 0.75;
        if ((this.vx * this.vx + this.vy * this.vy) > params.smashSpeedThreshold) {
          rockHits[0].obj.smash(this.vx, this.vy);
        }
      }
    }
    if (this.originX() < Crafty.viewport.bounds.min.x) {
      this.vx = Math.abs(this.vx);
      return true;
    }
    if (this.originX() > Crafty.viewport.bounds.max.x) {
      this.vx = -Math.abs(this.vx);
      return true;
    }
    return false;
  },

  updateVelocity: function() {
    this.vy += params.gravityForce;
    var distCopter = Crafty.math.distance(this.originX(), this.originY(), this.copter.originX(), this.copter.originY());
    if (distCopter !== this.ropeEqLength) {
      var Dx = this.originX() - this.copter.originX(); // true x-distance
      var dx = Dx * (1 - (this.ropeEqLength / distCopter)); // dx is the x-amount the rope is stretched.
      this.vx -= dx * this.ropeStiffness;
      var Dy = this.originY() - this.copter.originY();
      var dy = Dy * (1 - (this.ropeEqLength / distCopter));
      this.vy -= dy * this.ropeStiffness;

      this.copter.vx += 0.005 * dx;
      this.copter.vy += 0.005 * dy;
      this.x -= 0.1 * dx;
      this.y -= 0.1 * dy;
    }

    this.vx *= this.slowfactor;
    this.vy *= this.slowfactor;
    if (this.vx > -0.001 && this.vx < 0.001) {
      this.vx = 0;
    }
  },
});