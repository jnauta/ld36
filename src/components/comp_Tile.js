Crafty.c("Tile",{
  init: function() {
    this.requires('2D, Canvas, Collision, Particles');//.color('red');
    this.w = this.h = params.tileSize;    
  },
  
  _Tile: function(col, row, type){
    this.col = col;
    this.row = row;
    this.x =  params.tileSize * this.col;
    this.y =  params.tileSize * this.row;
    this.type = type;

    if (type === 'rock' || type === 'air') {
      this.shade = Crafty.e('2D, Canvas, shade').attr({x:this.x, y:this.y, z:params.zLevels['rock'] + 2, visible:false});
      this.attach(this.shade);   
      this.z = params.zLevels['rock'];
      this.attach(this.LU = Crafty.e('RockTileCorner')._RockTileCorner('LU',this.col,this.row).attr({x:this.x, y:this.y}));
      this.attach(this.RU = Crafty.e('RockTileCorner')._RockTileCorner('RU',this.col,this.row).attr({x:this.x+(params.tileSize / 2), y:this.y}));
      this.attach(this.RB = Crafty.e('RockTileCorner')._RockTileCorner('RB',this.col,this.row).attr({x:this.x+(params.tileSize / 2), y:this.y+(params.tileSize / 2)}));
      this.attach(this.LB = Crafty.e('RockTileCorner')._RockTileCorner('LB',this.col,this.row).attr({x:this.x, y:this.y+(params.tileSize / 2)}));
    } else if (type === 'grass') {
      this.requires('Grass');
      var rand = Math.floor(Math.random()*2);
      this.requires('grass').sprite(rand,0);
      this.z = params.zLevels['grass']
    }
    if(this.type === 'rock'){
      this.addComponent('Rock');
      this.checkHits('Hammer').bind("HitOn", function(hitData) {
        this.highlight();
      })
      .bind("HitOff", function(comp) {
        this.unhighlight();
      });

      // add some visual help for counting blocks
      // this.cracks = Crafty.e('2D, Canvas, crack').origin('center').attr({rotation: 90 * Math.floor(Math.random()*4), x:this.x, y:this.y, z:params.zLevels['rock'] + 1}).sprite(Math.floor(Math.random()*4),0);
      // this.attach(this.cracks);
    }
    return this;
  },

  smash: function(vx, vy) {
    var v = Math.sqrt(vx * vx + vy * vy);
    if(!mutesound){
      var rand = Math.floor(Math.random()*3,1,0.7);
      Crafty.audio.play('hit'+rand,1,0.5);}
    
    if(!player.timeIsUp){

      var addedPowerScore = 0;
      if (v > params.powerMaxV) {
        addedPowerScore= params.powerScore;
      } else {
        addedPowerScore = Math.floor(params.powerScore * (v / params.powerMaxV));
      }

      player.powerScore += addedPowerScore;
      flyingScoreText = Crafty.e('2D,Canvas,Text,Tween,Delay').text('+'+ addedPowerScore.toString()).attr({x:this.x, y:this.y, z: params.zLevels['rock']+1})
      .textColor('#000000').textFont({size: '40px',  family: fontFamily3}).tween({x:this.x+Math.random()*150-75, y : this.y + Math.random()*150-75, alpha:0},500).delay(function(){this.destroy()},600,0);
    }

    var arrayIdxs = [this._x / params.tileSize, this._y / params.tileSize];
    this.removeComponent('Rock');
    this.addComponent('Smashed');
    this.type = "air";
    this.unhighlight();
    this.shade.visible = false;
    // this.cracks.visible = false;
    for (var i = -1; i < 2; i++) {
      for (var j = -1; j < 2; j++) {
        var neighborIdxs = [this.col + i, this.row + j];
        if (neighborIdxs[0] >= 0 && neighborIdxs[0] < tiles.length && tiles.length > 0) {// &&
            //neighborIdxs[1] >= 0 && neighborIdxs[1] < tiles[0].length) {
          var neighborTile = tiles[neighborIdxs[0]][neighborIdxs[1]];
          if (neighborTile) { // some tiles are undefined ("air" tiles).
            neighborTile.updateSprite();
          }
        }
      };   
    };
    var matchResult = matchSculpture(this.col, this.row);

    if (matchResult !== null) {
      Crafty.trigger('sculpted', matchResult);
    }
    Crafty.e('2D, Canvas, Particles, Delay').attr({z:params.zLevels['rock']+1,x:this.x + (params.tileSize/2), y :this.y + (params.tileSize/2)}).particles(params.particleOptions).delay(function(){this.destroy()},2000,0);
    
  },

  highlight: function(){
    if(this.type === 'rock'){
      this.LU.alpha = 0.7;    
      this.RU.alpha = 0.7;
      this.RB.alpha = 0.7;
      this.LB.alpha = 0.7;
      this.shade.alpha = 0.7;
      // this.cracks.alpha = 0.7;
    }
  },

  unhighlight: function(){
    if(this.type === 'rock' || this.LU.alpha <1){
      this.LU.alpha = 1;    
      this.RU.alpha = 1;
      this.RB.alpha = 1;
      this.LB.alpha = 1;
      this.shade.alpha = 1;
      // this.cracks.alpha = 1;
    }
  },

  updateSprite:function() {
    if(this.type === 'grass'){return;}
    // check which surrounding tiles are rock, adjust own sprite.
    var arrayIdxs = [this._x / params.tileSize, this._y / params.tileSize];
    surroundingTiles = new Array(3);
    for(var i = 0 ; i<3; i++){
      surroundingTiles[i] = new Array(3);
      for(var j = 0 ; j<3; j++){
        var tileCol = tiles[this.col- 1 + i];
        if(tileCol){
          tile = tiles[this.col- 1 + i][this.row - 1 + j];

          if(tile){
            var type =  tiles[this.col- 1 + i][this.row - 1 + j].type;
            if (type === 'grass') {
              surroundingTiles[i][j] = 1;
            }
            if(type === 'air'){
              surroundingTiles[i][j] =0;
            } else if (type === 'rock'){
              surroundingTiles[i][j] =1;
            }
          }
          else{
           surroundingTiles[i][j] = 0; 
          }
        }
        else{
           surroundingTiles[i][j] = 0; 
        }
      }

    }
    if (surroundingTiles[1][1] === 0){ // if this is an air tile
      
      if(surroundingTiles[0][1]+surroundingTiles[1][0] === 2){
        this.LU.sprite(3,3);        
      }else{ this.LU.sprite(6,0);}
      if(surroundingTiles[1][0]+surroundingTiles[2][1] === 2){
        this.RU.sprite(3,3);
      }else{ this.RU.sprite(6,0);}
      if(surroundingTiles[2][1]+surroundingTiles[1][2] === 2){
        this.RB.sprite(3,3);
      }else{ this.RB.sprite(6,0);}
      if(surroundingTiles[1][2]+surroundingTiles[0][1] === 2){
        this.LB.sprite(3,3);
      }else{ this.LB.sprite(6,0);}
    }
    else{ // if this tile is still rock tile

      //check shading
      if(surroundingTiles[2][1] === 0 ){this.shade.visible = true};
      
      var arrayLU = [ surroundingTiles[0][1], surroundingTiles[0][0], surroundingTiles[1][0] ];
      var arrayRU = [ surroundingTiles[1][0], surroundingTiles[2][0], surroundingTiles[2][1] ];
      var arrayRB = [ surroundingTiles[2][1], surroundingTiles[2][2], surroundingTiles[1][2] ];
      var arrayLB = [ surroundingTiles[1][2], surroundingTiles[0][2], surroundingTiles[0][1] ];
      
      this.LU.updateSprite(arrayLU);
      this.RU.updateSprite(arrayRU);
      this.RB.updateSprite(arrayRB);
      this.LB.updateSprite(arrayLB);
      
    }
  }
});

Crafty.c("RockTileCorner",{
  init: function(){
    this.requires('2D, Canvas, tile2'); 
    this.z = params.zLevels['rock']  ;
    this.h = params.tileSize/2;
    this.w = params.tileSize/2;
    this.origin('center');

  },

  _RockTileCorner: function(cornerType,col,row){
    this.col = col;
    this.row = row;
    this.cornerType = cornerType;
    
    switch(this.cornerType){
      case 'LU':
        this.sprite(1,1);
        this.rotation = -90;
        break;
      case 'RU':
        this.sprite(1,1);
        break;
      case 'RB':
        this.sprite(1,1);
        this.rotation = 90;
        break;
      case 'LB':
        this.sprite(1,1);
        this.rotation = 180;
        break;
    };
    return  this;
  },

  updateSprite: function(array){
  

    if(array[0]===0){
      if(array[1]===0){
        if(array[2]===0){
          // 0,0,0
          this.sprite(1,0);
        }else if(array[2]===1){
          // 0,0,1    
          this.sprite(0,0);      
        } 
      }else if(array[1]===1){
        if(array[2]===0){
          // 0,1,0
          this.sprite(2,1);
        }else if(array[2]===1){
          // 0,1,1
           this.sprite(0,3);
        }
      }

    }else if(array[0]===1){
      if(array[1]===0){
        if(array[2]===0){
          // 1,0,0
          this.sprite(1,1);
        }else if(array[2]===1){
          // 1,0,1        
          this.sprite(0,1);  
        } 
      }else if(array[1]===1){
        if(array[2]===0){
          // 1,1,0
          this.sprite(1,3);
        }else if(array[2]===1){
          // 1,1,1
          this.sprite(0,1)
        }
      }
    }

  }

});
