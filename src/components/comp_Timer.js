Crafty.c('Timer',{
	init: function(){
		this.requires('2D, HTML, Keyboard,  Delay').attr({x:600, y:200,z:params.zLevels['overlay'] + 1}).css({'color':'#000000'});
		this.borderPx = 2;
		this.css({'color':mycolors.clockTextColor,'font-size':'40px', 'text-align': 'center', 'border': this.borderPx + 'px solid black','border-radius': '10px', 'background-color':mycolors.clockBg, 'font-family' : fontFamily3 });
		
		this.w = 4*25 + 3*8;
		this.h = 50;
		this.alpha = 0.8;
		

		this.timeLeft = 10;
		
		
		
		// console.log('time left : '+ this.timeLeft);

		

	},

	_Timer: function(){
		this.viewportDiffX = 800 - this.borderPx*4 - this.w ; // 800 - this.borderPx*8 - this.w -150;
		this.viewportDiffY = 6 * this.borderPx + this.w ; 

		
		this.x = -Crafty.viewport._x + this.viewportDiffX;
		this.y =-Crafty.viewport._y + this.viewportDiffY;
		
		// this.attach(this.e);

		
		this.bind('EnterFrame', function() {
			// Make relative to viewport
			this.x = -Crafty.viewport._x + this.viewportDiffX;
			this.y = -Crafty.viewport._y + this.viewportDiffY;	
			
		
		
		
		});	

		

		return this;
	},

	showTime: function(){
		var seconds = (this.timeLeft % 60).toString();
		if(seconds.length === 1){seconds = '0'+ seconds}
		var minutes = Math.floor(this.timeLeft/60).toString();
		this.replace(minutes + ":" + seconds);
		// this.timerDiv.innerHtml = " " + minutes + ":" + seconds + " ";
	},

	timeIsUp: function(){
		console.log('Time is up!');
		this.showTimeIsUpText();
		player.timeIsUp = true;
		judgement = judgeSculpture();
		player.sculptureScore = judgement[0];
		player.powerScore = Math.floor(player.powerScore);
		var maxScores = calculateMaxScores(currentSculptureMatrix);
		if (maxScores.sculptureScore === 0) {
			console.log('ERROR, some score should be possible on the sculpture');
		}
		player.sculptureRating = Math.floor(100 * Math.max(0, (1 / 0.3) * (player.sculptureScore / maxScores.sculptureScore - 0.7))); // around 70% is usually free.
		if (player.sculptureRating === 100) {
			// smash the tiles that make up the sculpture...
			var offsets = [judgement[1], judgement[2]];
		  for (var sCol = 0; sCol < currentSculptureMatrix.length; ++sCol) {
		    for (var sRow = 0; sRow < currentSculptureMatrix[sCol].length; ++sRow) {
		      if (currentSculptureMatrix[sCol][sRow] === 1) {
		        var rockTile = tiles[offsets[0] + sCol][offsets[1] + sRow];
		        rockTile.smash(0, 0);
		      }
		    }
		  }

			// place the image over the sculpture
			var overlayImg = Crafty.e('2D, Canvas, Image').image(imageMap[currentSculpture.properties.image]) //.attr({x: 200, y: 500, w: 400, h: 400});
					.attr({x: judgement[1] * params.tileSize, y: judgement[2] * params.tileSize, z: params.zLevels.rockImage, w: currentSculpture.width * params.tileSize, h: currentSculpture.height * params.tileSize});
		}
		var levelScore = Math.floor((player.sculptureScore + player.powerScore));
		// var outputString = myTexts.levelFinished + '<br><span class = "scoreheader">Sculpting</span><br>accuracy: ' + player.sculptureRating
  // 										+ '%<br>score: ' + player.sculptureScore + '<br><span class = "scoreheader">Smashing</span><br>score: ' + player.powerScore
  // 										+ '<br><span class = "scoreheader">Total: ' + levelScore + '</span><br>';
  		var outputString = myTexts.levelFinished + '<br><br><span class = "scoreheader">Score:</span><br>Sculpting: ' + player.sculptureScore 
  												 + '<br><span >Smashing</span>: ' + player.powerScore
  												+ '<br> Accuracy: ' + player.sculptureRating + '%'
  												+ '<br><span class = "scoreheader">Total: ' + levelScore + '</span><br>';

	  	if (player.sculptureRating >= 60) { // pass to next level, if any
		  	if(sculptureIdx < sculptures.length -1){ // next level
		  		buttons.helpText(outputString + '<br>'  + myTexts.levelPassed);
		  		this.bind('KeyDown', function(){
		  			if(this.isDown("ENTER")) {
		  				playerScore += levelScore;
		  				sculptureIdx += 1;
		  				if (overlayImg){
		  					overlayImg.destroy();
		  				}
		  				this.unbind();
		  				Crafty.scene('Main');	        	
		  			}
	      	});
			} else { // what if this was last level
				playerScore += levelScore;
				console.log('Player score is now ' + playerScore);
				buttons.helpText(outputString + '<br>' + myTexts.finished + playerScore + '</span><br><br>' + myTexts.finished2);
				this.bind('KeyDown',function(){
					if(this.isDown("ENTER")){
	    				sculptureIdx = 0;
	    				console.log('ENTER SETS TO ZERO');
	    				playerScore = 0;
	    				this.unbind();	        	
	    				Crafty.scene('Main');
	  				}		
	      	});
		    }
		} else { // fail! try this level
		  	buttons.helpText(outputString + '<br>' + myTexts.levelFailed);
		  	this.bind('KeyDown', function(){

	  			if(this.isDown("ENTER")) {
	  				console.log('Enter to try again');
	  				this.unbind();
	  				Crafty.scene('Main');	        	
	  			}

	      	});
	  	}


		

	},

	startTimer: function(seconds){
		this.timeIsRunning = true;
		this.timeLeft = seconds;
		this.showTime();
		this.waitASecond();
	},

	stopTimer: function(seconds){
		this.timeIsRunning = false;
		this.showTime();
		
	},

	waitASecond: function(){
		this.delay(function() { 
			judgement = judgeSculpture();
			var sculptureScore = judgement[0];
			maxScores = calculateMaxScores(currentSculptureMatrix);

			sculptureRating = Math.floor(100 * sculptureScore / maxScores.sculptureScore);
			if(this.timeIsRunning){ 
				
				this.timeLeft -= 1;
				var seconds = (this.timeLeft % 60).toString();
				if(seconds.length===1){seconds = '0'+ seconds}
				var minutes = Math.floor(this.timeLeft/60).toString();
				this.replace(minutes + ":" + seconds);
				if(this.timeLeft > 0){
					// Crafty.log("1000ms later");
					this.waitASecond();
					if(this.timeLeft<=10){
						//if(!mutesound){Crafty.audio.play('clock',1,0.5);}
						this.css({'color':mycolors.clockTicking,'font-size':'40px'});
						this.delay(function(){
							this.css({'color':mycolors.clockTextColor,'font-size':'40px'});
						},500,0)
						//this.timerDiv.style.backgroundColor = '#ff0000';
						// if((this.timeLeft%2) ===0){
						// }else{
						// 	this.css({'color':'000000'});
						// }
					}
				}
				else{this.css({'color':mycolors.clockTicking,'font-size':'40px'});
					this.timeIsUp();
				}
			}
			
		}, 1000, 0);			
	},


	showTimeIsUpText: function(){
		console.log('showing tim is up text?');
		this.timeIsUpText = Crafty.e("2D, DOM, Tween, Text, Mouse").attr({h:600,w:150,  x:600, y:300, alpha:1, textSize:25}).css({'text-align':'center'}).origin('center')
		.text("Time's up!").textColor('#ff0000').textFont({size: '20px', weight: 'bold' , type: 'italic', family: fontFamily2}).unselectable();
		
		this.timeIsUpText.viewportDiffX = this.viewportDiffX;//800 -200 ; // 800 - this.borderPx*8 - this.w -150;
		this.timeIsUpText.viewportDiffY = this.viewportDiffY;//100 ; 
		console.log(this.timeIsUpText.viewportDiffX,this.timeIsUpText.viewportDiffY);

		

		this.timeIsUpText.attr({x:  -Crafty.viewport._x + this.timeIsUpText.viewportDiffX, y:  -Crafty.viewport._y + this.timeIsUpText.viewportDiffY});	

		// // this.tween({this.x-x:200}, 1000, "smoothStep");
		// // this.tween({this.x-x:200}, 1000, "smootherStep");
		// // this.tween({this.x-x:200}, 1000, "easeInQuad");
		 this.timeIsUpText.tween({viewportDiffX:80, viewportDiffY:130, w:600, h:100, textSize:100, rotation:-10}, 500, "easeOutQuad");
		 this.timeIsUpText.tween({alpha:1}, 500,  "smootherStep");
		// // this.tween({x:this.x-200}, 1000,  function(t){return 2*t*t - t;});
		

		this.bind('EnterFrame', function(){

			this.timeIsUpText.x = -Crafty.viewport._x + this.timeIsUpText.viewportDiffX;
			this.timeIsUpText.y  = -Crafty.viewport._y + this.timeIsUpText.viewportDiffY;
			this.timeIsUpText.textFont({size: this.timeIsUpText.textSize.toString() + 'px'});
		});

	},
});
