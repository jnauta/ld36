Crafty.c('Buttons',{
	init: function(){
		this.requires('2D,DOM').attr({x:750, y:200,z:params.zLevels['overlay']});
		this.borderPx = 2; 
		this.cssText = {'background-color': mycolors.clockBg, 'cursor':'pointer', 'border': this.borderPx + 'px solid black','border-radius': '10px' };
		this.cssTextHelp = {'background-color': mycolors.clockBg,  'border': this.borderPx + 'px solid black','border-radius': '10px' };
		
		
		this.w = 1500;
		this.h = 500;
		this.viewportDiffX = 800 - this.borderPx*4 - this.w;
		this.viewportDiffY = 2 * this.borderPx ;
		this.x = 0;//-Crafty.viewport._x + this.viewportDiffX;
		this.y = 0;//-Crafty.viewport._y + this.viewportDiffY;
		this.visible = false;
		
		buttonsAlpha = 0.8;
		// the buttons! 

		this.musicButton = Crafty.e('2D,DOM,button, Mouse').css(this.cssText )
		.attr({alpha: buttonsAlpha,  x: - Crafty.viewport._x + 800 -25 - this.borderPx*4,  y:  182 + this.borderPx*6  - Crafty.viewport._y});
		this.musicButton.bind('Click',function(){
			if(mutemusic){
				mutemusic = false;
				this.sprite(0,0);
				bgMusic.play();
			}else{
				mutemusic = true;
				this.sprite(1,0);
				bgMusic.pause();
			}
		})
		if(mutemusic){this.musicButton.sprite(1,0); bgMusic.pause();};

		this.soundButton = Crafty.e('2D,DOM,button, Mouse').css(this.cssText ).sprite(4,0)
		.attr({alpha: buttonsAlpha,  x: - Crafty.viewport._x + 800 -50 - this.borderPx*8,  y:  182 + this.borderPx*6  - Crafty.viewport._y});
		this.soundButton.bind('Click',function(){
			if(mutesound){
				mutesound = false;
				this.sprite(4,0);
				
			}else{
				mutesound = true;
				this.sprite(5,0);
				
			}
		})
		if(mutesound){this.musicButton.sprite(5,0); };
		
		this.resetButton = Crafty.e('2D,DOM,button, Mouse').css(this.cssText )
		.attr({alpha: buttonsAlpha, x: - Crafty.viewport._x + 800 -75 - this.borderPx*12,  y:  182 + this.borderPx*6  - Crafty.viewport._y}).sprite(3,0);
		this.resetButton.bind('Click',function(){resetLevel()});

		// this.settingsButton = Crafty.e('2D,DOM,button, Mouse').css(this.cssText )
		// .attr({alpha: buttonsAlpha,  x: - Crafty.viewport._x + 800 -75 - this.borderPx*12,  y:  182 + this.borderPx*6  - Crafty.viewport._y}).sprite(4,0);
		// this.settingsButton.bind('Click',function(){
		// 	console.log('for now nothing happens. Could also be back to start button. dunno.');
		// })
		
		this.helpButton = Crafty.e('2D,DOM,button, Mouse, Keyboard').css(this.cssText )
		.attr({alpha: buttonsAlpha, x: - Crafty.viewport._x + 800 -100 - this.borderPx*16,  y:  182 + this.borderPx*6  - Crafty.viewport._y}).sprite(2,0);
		this.helpWindow = Crafty.e('2D,DOM,Text').textColor('#000000')
		.textFont({size: '20px',  family: fontFamily2}).text(myTexts.helpText).css(this.cssText )
		.css({'text-align': 'left', 'padding':'5px'})
		.attr({alpha: buttonsAlpha, h: 354, w: 200, x: - Crafty.viewport._x + 800 - 200 -8 -10,  y: 182+25+8 + this.borderPx*6  - Crafty.viewport._y, visible : true});
		
			

		this.helpTextShow = function(){
			this.helpText(myTexts.helpText);
			this.helpWindow.visible = true;
		};
		this.helpTextHide = function(){
			this.helpWindow.visible = false;
		};
		this.helpText = function(wantedText){
			this.helpWindow.text(wantedText);
			this.helpWindow.visible = true;
		};

		this.helpButton.bind('MouseOver',function(){Crafty.trigger('ShowHelp')} ) ;
		this.helpButton.bind('MouseOut', function(){Crafty.trigger('HideHelp')} );
		this.helpButton.bind('KeyDown', function(){if(this.isDown('ENTER') || (this.isDown('SPACE')&&!player.timeIsUp) ){Crafty.trigger('HideHelp')}} );

		this.bind('ShowHelp',function(){this.helpTextShow();})
		this.bind('HideHelp',function(){this.helpTextHide();})

		this.attach(this.musicButton);
		this.attach(this.soundButton);
		this.attach(this.helpButton);
		this.attach(this.resetButton);
		// this.attach(this.settingsButton);
		this.attach(this.helpWindow);

		
		
	},

	_Buttons: function(){		
		this.bind('EnterFrame', function() {			
			this.x = -Crafty.viewport._x ;
			this.y = -Crafty.viewport._y ;
		});	

		return this;
	},

	

	



})