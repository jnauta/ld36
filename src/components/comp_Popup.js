Crafty.c("Popup", {
	init: function() {
		this.requires("2D, DOM,Text, Mouse, Keyboard, Tween");
		this.attr({x:200,alpha:0.8,y:100,h:120,w:400,z:1000}) ;
		this.textFont({size: '20px',  family: fontFamily2});
		this.css({'border-radius':'10px','cursor': 'default' ,'background-color':mycolors.popupBg,'color':mycolors.popupText,'border':'2px solid' + mycolors.popupBorder ,'padding':'20px'});
		// this.button = Crafty.e('2D, DOM,  Text, Mouse').attr({x:240,y:240,h:40,w:120,z:1110})
		// .css({'text-align':'center', 'color':mycolors.popupButtonText,'background-color':mycolors.popupButtonBg,'cursor':'pointer','border-radius':'5px'})
		// .textFont({size: '32px',  family: 'Impact'})
		// .bind('MouseOver',function(){this.css({	'background-color':mycolors.button1Hover, })})
		// .bind('MouseOut',function(){this.css({	  'background-color':mycolors.button1, })});
		// this.button.nr = 0;
		// this.attach(this.button);
		this.bind('hideText', function(){ this.visible = false });
		
		this.showText = function(text){
			this.text(text);
			this.button.text('Let\'s start!');
		};
		// Crafty.pause(1);
	},
	
	_Popup : function(text){
		this.text(text);
		this.bind('EnterFrame', function() {
			// Make relative to viewport
			this.x = -Crafty.viewport._x + 200;
			this.y = -Crafty.viewport._y + 400;

		});	
		
		
		return this;
	}
	
});
