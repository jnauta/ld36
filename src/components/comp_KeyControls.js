Crafty.c('KeyControls', {
	goingLeft: false,
	goingRight: false,
	
	init: function() {
		this.addComponent('Moving')._Moving();
		

		this.bind('KeyDown', function(keyEvent) {
			var k = keyEvent.key;
			if (k === this.up ) {
				this.lift = true;
				if(!mutesound){
					Crafty.audio.play('lift',-1,1);
				}
			} else if (k === this.down) {
				this.dive = true;
			} else if (k === this.left) {
				this.goingLeft = true;				
			} else if (k === this.right) {
				this.goingRight = true;				
			} else if (k === this.smash) {
				this.hammer.smashing = true;
			}
		});
		this.bind('KeyUp', function(keyEvent) {
			var k = keyEvent.key;
			if (k === this.up ) {
				this.lift = false;
				if(!mutesound){Crafty.audio.stop('lift');}
			} else if (k === this.down) {
				this.dive = false;
			} else if (k === this.left) {
				this.goingLeft = false;
			} else if (k === this.right) {
				this.goingRight = false;
			} else if (k === this.smash) {
				this.hammer.smashing = false;
			}
		});
		
	},
	_KeyControls: function(left, right, up, down, smash) {
		this.left = left;
		this.right = right;
		this.up = up;
		this.down = down;
		this.smash = smash;
		this.xAcc = params.playerXAcceleration;
		this.upAcc = params.playerUpAcc;
		this.downAcc = params.playerDownAcc;

		this.wobbleMinY = 400;
		this.wobbleMaxY = 500; 
		this.wobbleMinX = 100; 
		this.wobbleMaxX = 150;

		return this;
	},
	
	

	wobble: function(){
		this.vy += 0.3 * params.gravityForce;
		var airDragY;
		if (this.vy < 0) {// going up, we experience less drag because we use the rotor
			airDragY = this.vy * params.playerAirDragY * 0.5;//(this.vy + params.playerMaxLift) * params.playerAirDragY;
		} else {
			airDragY = this.vy * params.playerAirDragY;
		}
		this.vy -= airDragY;
		var airDragX = this.vx * params.playerAirDragX;
	    this.vx -= airDragX;

	    if(this.y > this.wobbleMaxY){this.lift= true, this.dive = false;}
	    if(this.y < this.wobbleMinY){this.lift = false, this.dive = true;}
	    if(Math.floor(Math.random()*30)===1 ){
	    	if(this.isGoingLeft){
	    		this.goingRight=true; 
				this.goingLeft = false;
			}else{
	    		this.isGoingLeft = true;
	    		this.goingRight = false; 
	    	}
	    }
		
		maxVx = 0.1;
		
		if(this.vx > maxVx){this.vx = maxVx}
		if(this.vx < -maxVx){this.vx = -maxVx}
		
	    
	    if(this.x > this.wobbleMaxX){this.goingLeft = true; this.goingRight = false; }
	    if(this.x < this.wobbleMinX){this.goingRight=true; this.goingLeft = false;}
		
	    

	},

	updateVelocity: function() {
		// add jerk to acc to simulate the starting and slowing of the rotor.
		//this.yAcc += this.jerk;
		this.vy += 0.3 * params.gravityForce;
		var airDragY;
		if (this.vy < 0) {// going up, we experience less drag because we use the rotor
			airDragY = this.vy * params.playerAirDragY * 0.5;//(this.vy + params.playerMaxLift) * params.playerAirDragY;
		} else {
			airDragY = this.vy * params.playerAirDragY;
		}
		this.vy -= airDragY;
		var airDragX = this.vx * params.playerAirDragX;
	    this.vx -= airDragX;
			//}
		if (this.lift) {
			this.vy -= this.upAcc;
		}
		if (this.dive) {
			this.vy += this.downAcc;
		}
		if (this.goingRight) {
	      this.vx += this.xAcc;
	    }
	    if (this.goingLeft) {
	      this.vx -= this.xAcc;
	    }
	    if (this.vx > -0.001 && this.vx < 0.001) {
	      this.vx = 0;
	    }
    // weird: the next line makes the rope dissapear when rotated :S
    // this.rotation = Math.floor(this.vx * 3);
    
    	// if(this.wobbling){this.wobble();}
    
    
  }

});