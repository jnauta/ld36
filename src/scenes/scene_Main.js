var overlay;
bgMusic = null;
//Crafty.audio.add('bgMusic', 'assets/sound/bgmusic.ogg');

var generateTiles = function(sculptureMatrix) {
  var mountain = dilateSculpture(sculptureMatrix, currentSculpture.width, currentSculpture.height, params.edgeTiles);
  var mountainTileHeight = currentSculpture.height + params.edgeTiles;
  var mountainTileWidth = currentSculpture.width + 2 * params.edgeTiles;


  grassTileRow = (mountainTileHeight + params.levelTilesTop);
  playerSpawnY = grassTileRow *  tileSize - 170;

  // // print mountain
  // for (var i = 0; i < mountain.length; ++i) {
  //   console.log(mountain[i]);
  // }
  var tiles = [];

  for (var col = 0; col < mountainTileWidth + params.levelTilesLeft + params.levelTilesRight; ++col) {
    tiles[col] = new Array();
    for (var row = 0; row < mountainTileHeight + params.levelTilesTop + params.levelTilesBottom; ++row) {
      // create tile in sculpture
      if (row >= params.levelTilesTop && row < mountainTileHeight + params.levelTilesTop &&
          col >= params.levelTilesLeft && col < mountainTileWidth + params.levelTilesLeft) {
        var tileIdx = mountain[col - params.levelTilesLeft][row - params.levelTilesTop];
        if ((tileIdx || tileIdx === 0) && (row < mountainTileHeight + params.levelTilesTop)) {
          if (tileIdx - firstTileID === 0) {
            tiles[col][row] = Crafty.e('Tile')._Tile(col,row, 'rock');
          } else if (tileIdx - firstTileID === -1) {
            tiles[col][row] = Crafty.e('Tile')._Tile(col,row, 'air'); // absent tiles are air.
          }
        }
      }
      if (row === mountainTileHeight + params.levelTilesTop) {
        tiles[col][row] = Crafty.e('Tile')._Tile(col, row, 'grass');
      }
      else if (row > mountainTileHeight + params.levelTilesTop) {
        var rand2 = Math.floor(Math.random()*2);        
        var rand3 = Math.floor(Math.random()*3);        
        tiles[col][row] = Crafty.e('Tile')._Tile(col, row, 'grass').sprite(rand2,1 + rand3);
      }
      // tileIdx = bgLayer.data[currentSculpture.width * row + col];
      // if (tileIdx != 0) {
      //  Crafty.e('2D, Canvas, Background')._Background(col, row).sprite((tileIdx - 1) % xTilesInSprite, Math.floor((tileIdx - 1) / xTilesInSprite));
      // }
    }
  }
  return tiles;

};


resetLevel = function(){
  // Crafty('Smashed').each(function(){
  //   console.log('smashed');
  //   this.type === 'rock';
  //   this.updateSprite();
  // });

  
  Crafty.scene('Main');
};



matrixFromArray = function(array, width, height) {
  var matrix = new Array();
  for (var col = 0; col < width; ++col) {
    matrix[col] = new Array();
    for (var row = 0; row < height; ++row) {
        var tileIdx = array[currentSculpture.width * row + col];
        matrix[col][row] = tileIdx;
    }
  };
  return matrix;
},


dilateSculpture = function(inputSculptureMatrix, width, height, edgeTiles) {


  var tileMatrix = inputSculptureMatrix.map(function(row){return row.slice()});

  // add rows to top, left and right.
  for (var i = 0; i < edgeTiles; i++) {
    tileMatrix.unshift(new Array());
    tileMatrix.push(new Array());
    for (var row = 0; row < height; row++) {
      tileMatrix[0].push(0); // right
      tileMatrix[tileMatrix.length - 1].push(0); // left
    };
  };
  for (var i = 0; i < edgeTiles; i++) {
    for (var j = 0; j < width + 2 * edgeTiles; j++) {
      // the matrix is filled, no weird missing indices
      tileMatrix[j].unshift(0);
    };
  };

  // for (var i = 0; i < tileMatrix.length; ++i) {
  //   console.log(tileMatrix[i]);
  // }


  // resized the array, now dilate

  for (var iter = 0; iter < edgeTiles; iter++) {
    for (var col = 0; col < width + 2 * edgeTiles; ++col) {
      for (var row = 0; row < height + edgeTiles; ++row) {
        var tileIdx = tileMatrix[col][row];
        if (tileIdx === 0) {
          // check neighbors
          //var surroundingTiles = new Array(3);
          for(var i = 0 ; i<3; i++){
            //surroundingTiles[i] = new Array(3);
            for(var j = 0 ; j<3; j++){
              var tileCol = tileMatrix[col- 1 + i];
              if(tileCol){
                var neighbor = tileMatrix[col- 1 + i][row - 1 + j];
                if ((neighbor === 1 || neighbor === 2) && Math.random() > 0.7) {
                  tileMatrix[col][row] = -1; // temporary value to prevent influence
                }
              }
            }
          }
        }
      }
    }
    for (var col = 0; col < width + 2 * edgeTiles; ++col) {
      for (var row = 0; row < height + edgeTiles; ++row) {
        var t = tileMatrix[col][row];
        if (t === -1 || t === 2) {
          tileMatrix[col][row] = 1;
        }
      }
    }
  };
  //tileIdx = 1; // make all "need to remove"-tiles rocks
  return tileMatrix;
}

buildLevel = function() {
  // info = Crafty.e('Info');
  currentSculpture = TileMaps[sculptures[sculptureIdx]];

  var tileSets = currentSculpture.tilesets;
  var xTilesInSprite, yTilesInSprite;
  rockLayer = null;
  bgLayer = null;
  
  for (var i = tileSets.length - 1; i >= 0; i--) {
    var t = tileSets[i];
    if (t.name === "rockandair") {
      // these are the real tiles; solid stuff, background etc.
      tileSize = t.tilewidth;
      xTilesInSprite = t.imagewidth / t.tilewidth;
      yTilesInSprite = t.imageheight / t.tileheight;
      firstTileID = t.firstgid;
    }
  };

  for (var i = currentSculpture.layers.length - 1; i >= 0; i--) {
    var layer = currentSculpture.layers[i];
    if (layer.name === "rock") {
      rockLayer = layer;
    } else if (layer.name === "background") {
      bgLayer = layer;
    }
  }

  currentSculptureMatrix = matrixFromArray(rockLayer.data, currentSculpture.width, currentSculpture.height);

  overlay = Crafty.e('OverlaySculpture')._OverlaySculpture(currentSculpture);
  timer = Crafty.e('Timer')._Timer();

  buttons = Crafty.e('Buttons')._Buttons();

  var popupText = '';
  if(sculptureIdx===0){
    buttons.helpText(  myTexts.sculpture0);
  }
  else{
    buttons.helpTextShow();
  }
  
  
  


  tiles = generateTiles(currentSculptureMatrix);

  // update all sprites to look nice.
  for (var i = tiles.length - 1; i >= 0; i--) {
    for (var j = tiles[i].length - 1; j >= 0; j--) {
      var tile = tiles[i][j];
      if (tile && (tile.type === 'rock' || tile.type === 'air')) {
        tile.updateSprite();
      }
    }
  }
  player = Crafty.e('Player')._Player([100, playerSpawnY], 'right');
  

  

  Crafty.bind('sculpted', function(tileOffsets){
    console.log('sculpture complete!');
    // var marker = Crafty.e('2D, Canvas, Color').color('red');
    // marker.x = tileOffsets[0] * tileSize;
    // marker.y = tileOffsets[1] * tileSize;
    // marker.w = tileSize * 2;
    // marker.h = tileSize * 2;
  })

}

// col, row index the position of a tile that just changed type w.r.t. tiles.
// returns [column, row] of the top left corner of the sculpture in world-coordinates if there is a match, null otherwise.
matchSculpture = function(col, row) {
  var baseTile = tiles[col][row];
  var sculptureData = currentSculpture.layers[0].data;
  var offsets;
  var success;
  for (var i = currentSculpture.width - 1; i >= 0; i--) {
    for (var j = currentSculpture.height - 1; j >= 0; j--) {
      //if (currentSculpture[i][j] && currentSculpture[i][j].type === baseTile.type) {
        // at least the changed tile is the same as the currentSculpture at this position.
        // check whether the rest corresponds as well.
        // set offsets so that currentSculpture(0,0) === tiles(offsets).
        offsets = [col - i, row - j];
        success = true;
        for (var sCol = 0; sCol < currentSculpture.width; ++sCol) {
          for (var sRow = 0; sRow < currentSculpture.height; ++sRow) {
            var sTile = sculptureData[sCol + sRow * currentSculpture.width];
            if (!sTile) {
            // if there is no tile in the sculpture, anything goes in tiles.
              continue;
            } else {
              var tile;
              if (sCol + offsets[0] < tiles.length && sCol + offsets[0] > 0) {
                tile = tiles[sCol + offsets[0]][sRow + offsets[1]];
              }
              var sTileIsSolid = currentSculpture.tilesets[0].tileproperties[sTile - firstTileID].solid === "yes";
              // console.log(currentSculpture.tilesets[0].tileproperties[sTile - firstTileID].solid);
              // if (tile) console.log(tile.type + 'tiletype');
              if ((sTileIsSolid && (tile && tile.type !== "rock") || !tile) || (!sTileIsSolid && tile && tile.type === 'rock')) {
                success = false;
                break;
              }
            }
          }
          if (!success) {
            break;
          }
        }
        if (success) {
          return [col - i, row - j];
        }
    }
  }
  return null;
}

judgeSculpture = function() {
  var minCol = params.levelTilesLeft - Math.ceil(currentSculpture.width / 2);
  var maxCol = params.levelTilesLeft + (currentSculpture.width + 2 * params.edgeTiles) - Math.floor(currentSculpture.width / 2);
  var minRow = params.levelTilesTop - Math.ceil(currentSculpture.height / 2);
  var maxRow = params.levelTilesTop + (currentSculpture.height + params.edgeTiles) - Math.floor(currentSculpture.height / 2);
  //var sRockCount = 0;
  var maxScore = 0;
  var bestCol = bestRow = 0;
  //Crafty('JAREA').each(function(){this.destroy()});
  //Crafty.e('2D, Canvas, Color, JAREA').color('blue').attr({x: minCol * params.tileSize, y: minRow * params.tileSize, z: 10000, w: (maxCol - minCol) * tileSize, h: (maxRow - minRow) * tileSize, alpha: 0.2});

  // count amount of rock, we will require a fraction of this.
  // for (var sCol = 0; sCol < currentSculptureMatrix.length; ++sCol) {
  //   for (var sRow = 0; sRow < currentSculptureMatrix[sCol].length; ++sRow) {
  //     if (currentSculptureMatrix[sCol][sRow] === 1) {
  //       ++sRockCount;
  //     }
  //   }
  // }
  // for (var i = 0; i < currentSculptureMatrix.length; ++i) {
  //     console.log(currentSculptureMatrix[i]);
  // }
  for (var refCol = minCol; refCol < maxCol - 1; ++refCol) {
    for (var refRow = minRow; refRow < maxRow - 1; ++refRow) {
      var score = 0;
      var rockCount = 0;
      for (var sCol = 0; sCol < currentSculptureMatrix.length; ++sCol) {
        for (var sRow = 0; sRow < currentSculptureMatrix[sCol].length; ++sRow) {
          if (refRow + sRow < grassTileRow) {
            var sTile = currentSculptureMatrix[sCol][sRow];
            var wCol = tiles[refCol + sCol];
            var wTileObj = null;
            var wTile;
            if (wCol) {
              wTileObj = wCol[refRow + sRow];
              if (wTileObj && wTileObj.type === "rock") {
                wTile = 1;
              } else {
                wTile = 0;
              }
            }
            if (sTile === 1 && wTile === 1) {
              score += params.rockScore;
              //++rockCount;
            } else if (sTile === 2 && (!wTile || wTile !== 1)) {
              var goodRockNeighbors = 0;
              for(var i = 0 ; i<3; i++){
                //surroundingTiles[i] = new Array(3);
                for(var j = 0 ; j<3; j++) {

                  var wNeighborCol = tiles[refCol + sCol - 1 + i];
                  if(wNeighborCol){
                    var wNeighbor = wNeighborCol[refRow + sRow - 1 + j];
                    var sNeighborCol = currentSculptureMatrix[sCol - 1 + i];
                    if (sNeighborCol) {
                      sNeighbor = sNeighborCol[sRow -1 + j];
                      if (wNeighbor && wNeighbor.type === "rock" && sNeighbor === 1) {
                        ++goodRockNeighbors;
                      }
                    }
                  }
                }
              }
              if (goodRockNeighbors >= params.isolatedAirThres) {
                score += params.isolatedAirScore;
              } else {
                score += params.airScore;
              }
            }
          }
        }
      }
      if (score > maxScore) { //&& rockCount > sRockCount / 2) {
        bestCol = refCol;
        bestRow = refRow;
        maxScore = score;
      }
    }
  };
  // clear previous judgement
  Crafty('Judge').each(function(){this.destroy()});

  // draw the current sculpture superimposed
  // for (var sCol = 0; sCol < currentSculpture.width; ++sCol) {//currentSculptureMatrix.length; ++sCol) {
  //   for (var sRow = 0; sRow < currentSculpture.height; ++sRow) {//Matrix[sCol].length; ++sRow) {
  //     var tileIdx = currentSculptureMatrix[sCol][sRow];
  //     if (tileIdx === 1) {
  //       Crafty.e('2D, Canvas, Color, Judge').color('red').attr({x: (sCol + bestCol) * params.tileSize, y: (sRow + bestRow) * params.tileSize, z: 10000, w: tileSize, h: tileSize, alpha: 0.5});
  //     }
  //   }
  // }

  return [maxScore, bestCol, bestRow];
}

calculateMaxScores = function(sculptureMatrix) {
  var maxScores = {sculptureScore: 0, powerScore: 0};
  var rTiles = 0;
  var aTiles = 0;
  var iTiles = 0;
  for (var col = 0; col < currentSculpture.width; ++col) {//currentSculptureMatrix.length; ++sCol) {
    for (var row = 0; row < currentSculpture.height; ++row) {//Matrix[sCol].length; ++sRow) {
      var tileIdx = currentSculptureMatrix[col][row];
      if (tileIdx === 1) {
        rTiles++;
        maxScores.sculptureScore += params.rockScore;
        maxScores.powerScore += params.powerScore;
      } else if (tileIdx === 2) {
        var rockNeighbors = 0;
        // find out if eligible for isolatedAir
        for(var i = 0 ; i<3; i++){
          //surroundingTiles[i] = new Array(3);
          for(var j = 0 ; j<3; j++){
            var tileCol = sculptureMatrix[col- 1 + i];
            if(tileCol){
              var neighbor = tileCol[row - 1 + j];
              if (neighbor === 1) {
                ++rockNeighbors;
              }
            }
          }
        }
        if (rockNeighbors >= params.isolatedAirThres) {
          iTiles++;
          maxScores.sculptureScore += params.isolatedAirScore;
        } else {
          aTiles++;
          maxScores.sculptureScore += params.airScore;
        }
      }
    }
  }
  //console.log('rock ' + rTiles + ' air ' + aTiles + ' isolated ' + iTiles);
  return maxScores;
}

// LOADING SCENE
Crafty.scene('Main', function() {
  // Crafty.background('#F7EFD0');// url(assets/images/background.png) no-repeat center center');
Crafty.background('#FFFFFF url(assets/images/background.png) repeat center center');
  
  // Crafty.background(mycolors.background);
  Crafty.timer.FPS(60);
  //Crafty.viewport.bounds = {min:{x:0, y:0}, max:{x:Game.width(), y: Game.height()}};
  
  
  // if (!bgMusic) {
    // bgMusic = Crafty.audio.play('bgmusic', -1, 0.5);;

    // if(mutemusic && bgMusic.source){
    //   bgMusic.pause();
    // }
    
  // }
  

  buildLevel();
  
  player.sculptureScore = 0;
  player.powerScore = 0;
  timeForLevel = currentSculpture.properties.time; // in seconds

  timer.showTime(timeForLevel);
  timer.startTimer(timeForLevel);

  //Crafty.viewport.clampToEntities = false;
  var colTiles = tiles.length;
  var rowTiles = null;
  for (var i = tiles.length - 1; i >= 0; i--) {
    rowTiles = tiles[i].length > rowTiles ? tiles[i].length : rowTiles;
  };
  lowerViewBounds = {x: 0, y: 0};
  upperViewBounds = {x: colTiles * tileSize, y: rowTiles * tileSize};
  Crafty.viewport.bounds = {min: lowerViewBounds, max: upperViewBounds};
  Crafty.viewport.follow(player, 0, -100);//-player.originX(), -player.originY());

  // Title
  title = Crafty.e('2D, Canvas, Text, Mouse').attr({     x:  -Crafty.viewport._x + 50, y: -Crafty.viewport._y +10 ,   w:600, h:60 ,z:108  })
    .text('Helisculptor').textFont({ size: '44px',type: 'italic', family: fontFamily1}).textColor(mycolors.title).bind('EnterFrame',function(){
      this.x = -Crafty.viewport._x + 50 ;
      this.y = -Crafty.viewport._y + 10;
    });
    // .css({'padding-top': '5px','text-align':'center', 'text-shadow':'2px 2px '+ mycolors.titleshadow, 'color':mycolors.title});
  

  // player.wobbling = false;
  // player.wobbleMinY = player.y - 70;
  // player.wobbleMaxY = player.y ; 
  // player.wobbleMinX = player.x - 25; 
  // player.wobbleMaxX = player.x + 25;

  player.bind('KeyDown', function(){
    if(this.isDown('R')){
      resetLevel(); 
    }  
  });
 
  



});

