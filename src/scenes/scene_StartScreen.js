Crafty.scene('StartScreen', function() {

	// Crafty.scene('Main');
	// console.log('Startscreen');
	Crafty.timer.FPS(50);
   
    //background = Crafty.e('2D,DOM,Image').image('assets/images/background.jpg').attr({x:0, y:0, w:400,h:320, z:0});
    //Crafty.background(mycolors.background);
	 Crafty.background('url(assets/images/background.png)');
	// Crafty.background('#f4e8b8 url(assets/images/background.png) no-repeat center center');
	//Crafty.background('#FFFFFF url(assets/images/background2.jpg) no-repeat center center');
	
    DaVinciTestImage = Crafty.e('2D,DOM,SpriteAnimation,davinci').attr({x:200, y:150, w:220,h:180, z:0});
    DaVinciTestImage.reel('Flying',300,0,0,2);
    DaVinciTestImage.animate('Flying',-1);
    HammerTestImage = Crafty.e('2D,DOM,Image').image(imageMap['hammer']).attr({x:310, y:320, w:30,h:30, z:0,rotation:20});
    
    // Title
	Crafty.e('2D, DOM, Text, Mouse').attr({     x: 200,  y:0 ,   w:400, h:100 ,z:108  })
    .text('Helisculptor').textFont({ size: '63px',type: 'italic', family: fontFamily1})
		.css({'padding-top': '5px','text-align':'center', 'color':mycolors.title});

	Crafty.viewport.bounds = {min: [0,0], max: [100,100]};
	// player = Crafty.e('Player')._Player([100, 100], 'right');	

	Crafty.e('2D,DOM,Text,Mouse,Keyboard').attr({x:400, y:400, h:120,w:180}).text('Start').textFont({ size: '43px',type: 'italic', family: fontFamily1})
	.css({'cursor':'pointer', 'background-color':mycolors.stone,'border-radius':'5px','border':'2px solid black'})	
	.bind('Click',function(){ Crafty.scene('Main'); }		);
   
	
});
