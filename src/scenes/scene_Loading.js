// LOADING SCENE
Crafty.scene('Loading', function() {
	//Crafty.background('url(assets/images/background1.png)');
    // Crafty.background(mycolors.background);
    Crafty.background('url(assets/images/background.png)');
	
	Crafty.e('2D, DOM, Text')
		.attr({
			x: 250,
			y: 200,
			w: 300,
			h: 200,
			z: 8
		})
		.textFont({
			size: '24px',
			family: fontFamily1
		})
		.css({
			'padding-top': '45px',
			'background-color': mycolors.textBlockBg,
			'border': ('2px dashed' + mycolors.button1),
			'border-radius': '8px',
			'cursor': 'wait',
			'text-align': 'center',
			'padding-top': '3px',
			'color':mycolors.title,
		})
		.text('Loading,<br><BR> please wait...');
	Crafty.load(assetsObject,
		function() {
			//when loaded
			console.log('everything loaded!');
				
			Crafty.sprite(15, 15, "assets/images/tiles.png", {
				tile: [0,0],	
			});

			Crafty.sprite(30, 30, "assets/images/tiles2.png", {
				tile2: [0,0],	
			});
			
			Crafty.sprite(60, 60, "assets/images/shade.png", {
				shade: [0,0],	
			});

			Crafty.sprite(60, 60, "assets/images/floor.png", {
				grass: [0,0],	
			});

			Crafty.sprite(25, 25, "assets/images/buttons.png", {
				button: [0,0],	
			});

			Crafty.sprite(220, 180, "assets/images/davinci1.png", {
				davinci: [0,0],	
			});

			Crafty.sprite(30, 30, "assets/images/hammer.png", {
				hammer: [0,0],	
			});
			// Crafty.scene('StartScreen');
			bgMusic = Crafty.audio.play('bgmusic', -1, 0.3);

			Crafty.scene('Main');
		},

		function(e) { // onProgress
			//console.log(e);
		},

		function(e) {
			console.log('loading error');
			console.log(e);
		}
	);
    
    
    

});