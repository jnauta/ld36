var soundMap = {
	hit1: ['assets/sound/hit1.mp3', 'assets/sound/hit1.ogg', 'assets/sound/hit1.wav'],
	hit0: ['assets/sound/hit0.mp3', 'assets/sound/hit0.ogg', 'assets/sound/hit0.wav'],
	lift: ['assets/sound/lift.mp3', 'assets/sound/lift.ogg', 'assets/sound/lift.wav'],
	hit2: ['assets/sound/hit2.mp3', 'assets/sound/hit2.ogg', 'assets/sound/hit2.wav'],
	bgmusic: ['assets/sound/bgmusic.mp3', 'assets/sound/bgmusic.ogg', 'assets/sound/bgmusic.wav'],
};