var assetsObject = {
"audio": {
	"hit1": ["hit1.mp3", "hit1.ogg"],
	"hit0": ["hit0.mp3", "hit0.ogg"],
	"lift": ["lift.mp3", "lift.ogg", "lift.wav"],
	"hit2": ["hit2.mp3", "hit2.ogg"],
	"bgmusic": ["bgmusic.mp3", "bgmusic.ogg", "bgmusic.wav"],
},
"images": [
	"background.png",
	"background2.jpg",
	"bgimage.png",
	"buttons.png",
	"colossos.png",
	"davinci1.png",
	"easter.png",
	"eiffeltower.png",
	"favoicon.png",
	"floor.png",
	"hammer.png",
	"horse.png",
	"rockandair.png",
	"scrshot.png",
	"shade.png",
	"skali.png",
	"sphinx.png",
	"temple.png",
	"tiles.png",
	"tiles2.png"],
};